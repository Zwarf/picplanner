/*
 * picplanner-application.c
 * Copyright (C) 2021 Zwarf <zwarf@mail.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/*
 * TODO:
 * Save window state (position? and size)
 * Startup loading time error?
 * Docking on side error?
 * Remove first-run or make a first run info dialog?
 */

#include "picplanner-application.h"
#include "window/picplanner-window.h"
#include "window/preferences-dialog/preferences-dialog.h"

struct _PicplannerApplication
{
  GtkApplication  parent_instance;
  GSettings       *settings;
};


G_DEFINE_TYPE (PicplannerApplication, picplanner_application, ADW_TYPE_APPLICATION)


static void
first_run_set_timezone (PicplannerApplication *application)
{
  gint64 timezone;
  GDateTime *date_time;

  date_time = g_date_time_new_now_local ();
  timezone = g_date_time_get_utc_offset (date_time);

  g_settings_set_double (application->settings, "time-zone", (double) timezone/3600/1000/1000);
  g_settings_set_boolean (application->settings, "first-run", false);

  g_date_time_unref (date_time);
}

/*
 * Inverte the color scheme (light/dark) dependent on which scheme is set globaly
 */
static void
picplanner_inverte_scheme (GSettings *settings,
                           gchar *key,
                           gpointer user_data)
{
  PicplannerApplication *app = user_data;
  AdwStyleManager *manager = adw_application_get_style_manager (ADW_APPLICATION (app));
  gboolean invert_scheme = g_settings_get_boolean (settings, "invert-scheme");

  (void) key;

  if (invert_scheme)
    {
      if (adw_style_manager_get_dark (manager))
          adw_style_manager_set_color_scheme (manager, ADW_COLOR_SCHEME_FORCE_LIGHT);
      else
          adw_style_manager_set_color_scheme (manager, ADW_COLOR_SCHEME_FORCE_DARK);
    }
  else
    {
      adw_style_manager_set_color_scheme (manager, ADW_COLOR_SCHEME_DEFAULT);
    }
}


PicplannerApplication *
picplanner_application_new (gchar *application_id,
                            GApplicationFlags  flags)
{
  return g_object_new (PICPLANNER_TYPE_APPLICATION,
                       "application-id", application_id,
                       "flags", flags,
                       NULL);
}


static void
picplanner_application_finalize (GObject *object)
{
  G_OBJECT_CLASS (picplanner_application_parent_class)->finalize (object);
}


static void
picplanner_application_activate (GApplication *app)
{
  GtkWindow *window;
  PicplannerApplication *self;
  /* It's good practice to check your parameters at the beginning of the
   * function. It helps catch errors early and in development instead of
   * by your users.
   */
  g_assert (GTK_IS_APPLICATION (app));
  /* Get the current window or create one if necessary. */
  window = gtk_application_get_active_window (GTK_APPLICATION (app));

  if (window == NULL)
    window = g_object_new (PICPLANNER_TYPE_WINDOW,
                           "application", app,
                           "default-width", 900,
                           "default-height", 800,
                           NULL);

  self = PICPLANNER_APPLICATION (app);
  picplanner_inverte_scheme (self->settings, NULL, self);

  /* Ask the window manager/compositor to present the window. */
  gtk_window_present (window);
}


static void
picplanner_application_class_init (PicplannerApplicationClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GApplicationClass *app_class = G_APPLICATION_CLASS (klass);

  object_class->finalize = picplanner_application_finalize;
  /*
   * We connect to the activate callback to create a window when the application
   * has been launched. Additionally, this callback notifies us when the user
   * tries to launch a "second instance" of the application. When they try
   * to do that, we'll just present any existing window.
   */
  app_class->activate = picplanner_application_activate;
}


/*
 * PicPlanner About Dialog
 */
static void
picplanner_application_show_about (GSimpleAction *action,
                                   GVariant      *parameter,
                                   gpointer       user_data)
{

  PicplannerApplication *self = PICPLANNER_APPLICATION (user_data);
  GtkWindow *window = NULL;
  const gchar *authors[] = {"Zwarf <zwarf@mail.de>", NULL};
  const gchar *artists[] = {"Zwarf <zwarf@mail.de>",
                            "Rajdeep Singha <rajdeepsingha@proton.me>", NULL};
  const gchar *copyright =
  "Copyright \xC2\xA9 2020 Zwarf\n\n"
  "License Map Data ODBL OpenStreetMap Contributors\nMap Imagery CC-BY-SA 2.0 OpenStreetMap";

  g_return_if_fail (PICPLANNER_IS_APPLICATION (self));
  (void) action;
  (void) parameter;

  window = gtk_application_get_active_window (GTK_APPLICATION (self));

  adw_show_about_window (window,
                         "application-icon", "de.zwarf.picplanner",
                         "application-name", "PicPlanner",
                         "developer-name", "Zwarf",
                         "version", "0.5.3",
                         "website", "https://gitlab.com/Zwarf/picplanner",
                         "issue-url", "https://gitlab.com/Zwarf/picplanner/-/issues/new",
                         "copyright", copyright,
                         "license-type", GTK_LICENSE_GPL_3_0,
                         "developers", authors,
                         "designers", artists,
                         "translator-credits", _("translator-credits"),
                         NULL);
}


/*
 * PicPlanner Preferences Dialog
 */
static void
picplanner_application_show_preferences (GSimpleAction  *action,
                                         GVariant       *parameter,
                                         gpointer       user_data)
{
  PicplannerPrefs *prefs;
  GtkWindow *window = NULL;

  (void) action;
  (void) parameter;

  window = gtk_application_get_active_window (GTK_APPLICATION (user_data));
  prefs = picplanner_prefs_new (PICPLANNER_WINDOW (window));
  gtk_window_present (GTK_WINDOW (prefs));
}


static void
picplanner_application_init (PicplannerApplication *self)
{
  GSimpleAction *quit_action;
  GSimpleAction *about_action;
  GSimpleAction *preferences_action;
  const char *accels[] = {"<primary>q", NULL};

  quit_action = g_simple_action_new ("quit", NULL);
  g_signal_connect_swapped (quit_action, "activate", G_CALLBACK (g_application_quit), self);
  g_action_map_add_action (G_ACTION_MAP (self), G_ACTION (quit_action));

  about_action = g_simple_action_new ("about", NULL);
  g_signal_connect (about_action, "activate", G_CALLBACK (picplanner_application_show_about), self);
  g_action_map_add_action (G_ACTION_MAP (self), G_ACTION (about_action));

  preferences_action = g_simple_action_new ("preferences", NULL);
  g_signal_connect (preferences_action, "activate", G_CALLBACK (picplanner_application_show_preferences), self);
  g_action_map_add_action (G_ACTION_MAP (self), G_ACTION (preferences_action));

  gtk_application_set_accels_for_action (GTK_APPLICATION (self), "app.quit", accels);

  self->settings = g_settings_new ("de.zwarf.picplanner");
  if (g_settings_get_boolean (self->settings, "first-run"))
    first_run_set_timezone(self);

  g_signal_connect (G_OBJECT (self->settings),
                    "changed::invert-scheme",
                    G_CALLBACK (picplanner_inverte_scheme),
                    self);
}
