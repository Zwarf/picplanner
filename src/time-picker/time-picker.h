/*
 * time-picker.h
 * Copyright (C) 2021 Zwarf <zwarf@mail.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <gtk/gtk.h>

G_BEGIN_DECLS

#define TIME_PICKER_TYPE (time_picker_get_type ())
G_DECLARE_FINAL_TYPE (TimePicker, time_picker, TIME, PICKER, GtkBox)

void
time_picker_set_date_time (TimePicker *time_picker,
													 GDateTime *date_time);

void
time_picker_set_time_zone (TimePicker *time_picker,
                           double      time_zone);

int
time_picker_get_year (TimePicker *time_picker);

int
time_picker_get_month (TimePicker *time_picker);

int
time_picker_get_day (TimePicker *time_picker);

int
time_picker_get_hour (TimePicker *time_picker);

int
time_picker_get_minute (TimePicker *time_picker);


TimePicker *time_picker_new (void);

G_END_DECLS
