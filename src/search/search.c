/*
 * search.c
 * Copyright (C) 2021 Zwarf <zwarf@mail.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "search/search.h"


/*
 * This function is mostly taken from the libgweather test application:
 * https://gitlab.gnome.org/GNOME/libgweather/-/blob/main/libgweather/tests/test_libgweather.c
 * and only adapted to my needs.
 * It starts with the offline gweather world location and iterates through every child.
 * If the child type is not a city it recursively calls itself again to iterate through this child.
 * If the next_child is not existent == NULL the while loop breaks.
 * If the child type is a city and contains the search string (needle) it adds this city to the
 * locations array.
 */
gboolean
find_loc_children (GWeatherLocation *location,
                   const char       *search_str,
                   GWeatherLocation **locations,
                   int              *iteration)
{
  g_autoptr (GWeatherLocation) child = NULL;
  while ((child = gweather_location_next_child (location, child)) != NULL)
    {
      if (gweather_location_get_level (child) == GWEATHER_LOCATION_CITY)
        {
          g_autofree char *location_string_merge = NULL;
	  g_autofree char *location_norm = NULL;
	  g_autofree char *location_string = NULL;
          const char *city = gweather_location_get_name (child);
          const char *region = gweather_location_get_name (gweather_location_get_parent (child));
          const char *country = gweather_location_get_country_name (child);

          if (gweather_location_get_level (gweather_location_get_parent (child))
              == GWEATHER_LOCATION_COUNTRY)
            {
               location_string_merge = g_strdup_printf ("%s, %s", city, country);
            }
          else
            {
              location_string_merge = g_strdup_printf ("%s, %s, %s",
                                                       city,
                                                       region,
                                                       country);
            }

          location_norm = g_utf8_normalize (location_string_merge,
                                                  strlen (location_string_merge),
                                                  G_NORMALIZE_ALL_COMPOSE);
          location_string = g_utf8_casefold (location_norm, strlen (location_norm));

          if (strstr (location_string, search_str))
            {
              locations[*iteration] = g_object_ref(child);
              (*iteration)++;
              if (*iteration>=NUM_SEARCH_RESULTS)
                {
                  return TRUE;
                }
            }
        }
      else
        {
          if (find_loc_children (child, search_str, locations, iteration))
            return TRUE;
        }
    }
  return FALSE;
}
