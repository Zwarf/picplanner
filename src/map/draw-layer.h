/*
 * draw-layer.h
 * Copyright (C) 2008-2009 Pierre-Luc Beaudoin <pierre-luc@pierlux.com>
 * Copyright (C) 2011-2013 Jiri Techet <techet@gmail.com>
 * Copyright (C) 2019 Marcus Lundblad <ml@update.uu.se>
 * Copyright (C) 2021 Zwarf <zwarf@mail.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/*
 * Code mainly taken from:
 * https://gitlab.gnome.org/GNOME/libshumate/-/blob/main/shumate/shumate-path-layer.h
 * https://gitlab.gnome.org/GNOME/libshumate/-/blob/main/shumate/shumate-marker-layer.h
 * This is a combination of the 'ShumatePathLayer' and 'ShumateMarkerLayer' classes adjusted for the needs of PicPlanner.
 */

#ifndef PICPLANNER_DRAW_LAYER_H
#define PICPLANNER_DRAW_LAYER_H

#include <gtk/gtk.h>
#include <shumate/shumate.h>

#include "marker.h"

G_BEGIN_DECLS

#define PICPLANNER_TYPE_DRAW_LAYER picplanner_draw_layer_get_type ()
G_DECLARE_DERIVABLE_TYPE (PicplannerDrawLayer, picplanner_draw_layer, PICPLANNER, DRAW_LAYER, ShumateLayer)

struct _PicplannerDrawLayerClass
{
  ShumateLayerClass parent_class;
};

PicplannerDrawLayer
*picplanner_draw_layer_new (ShumateViewport *viewport);

void
picplanner_draw_layer_add_marker (PicplannerDrawLayer *layer,
                                  PicplannerMarker    *marker);

void
picplanner_draw_layer_add_node_coordinates (PicplannerDrawLayer *layer,
                                            double              x,
                                            double              y);

void
picplanner_draw_layer_remove_all (PicplannerDrawLayer *layer);


GdkRGBA
*picplanner_draw_layer_get_stroke_color (PicplannerDrawLayer *layer);

void
picplanner_draw_layer_set_stroke_color (PicplannerDrawLayer  *layer,
                                        const GdkRGBA        *color);

gboolean
picplanner_draw_layer_get_stroke (PicplannerDrawLayer *layer);

void
picplanner_draw_layer_set_stroke (PicplannerDrawLayer  *layer,
                                  gboolean             value);

double
picplanner_draw_layer_get_stroke_width (PicplannerDrawLayer *layer);

void
picplanner_draw_layer_set_stroke_width (PicplannerDrawLayer  *layer,
                                        double               value);

G_END_DECLS

#endif


