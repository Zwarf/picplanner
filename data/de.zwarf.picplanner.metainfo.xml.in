<?xml version="1.0" encoding="UTF-8"?>
<!-- Copyright 2023 Zwarf -->
<component type="desktop-application">
	<id>de.zwarf.picplanner</id>

  <launchable type="desktop-id">de.zwarf.picplanner.desktop</launchable>

	<metadata_license>CC0-1.0</metadata_license>

	<project_license>GPL-3.0-or-later</project_license>

	<name>PicPlanner</name>

	<summary>Plan your next photo locations</summary>

  <content_rating type="oars-1.1" />

	<description>
		<p>
			Calculate the position of the sun,
			moon, and milky way to plan the perfect time to take a picture.
		</p>

		<p>
			People who love to photograph know, that the biggest problem is being at the right spot at the right time.
			Therefore, landscape pictures should be planned before visiting the place of choice. To plan the location it is important to know the position of the sun, moon, and sometimes also the milky way.
			Where to find the sun at which time is most of the time easy to guess but when is the sunset? And for the milky way normally nobody knows where to find it in the night sky at which time at a specific location.
			This small program should answer all these questions.
		</p>
	</description>

	<screenshots>
		<screenshot type="default">
			<caption>Overview with a map and some basic information</caption>
			<image type="source" width="1600" height="900">https://gitlab.com/Zwarf/picplanner/-/raw/main/screenshots/picplanner-1.png</image>
		</screenshot>
		<screenshot>
			<caption>The sun page</caption>
			<image type="source" width="1600" height="900">https://gitlab.com/Zwarf/picplanner/-/raw/main/screenshots/picplanner-2.png</image>
		</screenshot>
		<screenshot>
			<caption>The moon page</caption>
			<image type="source" width="1600" height="900">https://gitlab.com/Zwarf/picplanner/-/raw/main/screenshots/picplanner-3.png</image>
		</screenshot>
		<screenshot>
			<caption>The milky way page</caption>
			<image type="source" width="1600" height="900">https://gitlab.com/Zwarf/picplanner/-/raw/main/screenshots/picplanner-4.png</image>
		</screenshot>
	</screenshots>

	<developer_name>Zwarf</developer_name>

	<update_contact>zwarf@mail.de</update_contact>

	<url type="homepage">https://gitlab.com/Zwarf/picplanner</url>
	<url type="bugtracker">https://gitlab.com/Zwarf/picplanner/-/issues</url>

	<custom>
    <value key="Purism::form_factor">mobile</value>
	  <value key="GnomeSoftware::key-colors">[(140, 40, 40), (250, 95, 85)]</value>
  </custom>

	<supports>
    <control>keyboard</control>
    <control>pointing</control>
    <control>touch</control>
  </supports>

	<requires>
    <display_length compare="ge">360</display_length>
  </requires>

  Date format following system default
	<releases>
	  <release version="0.5.3" date="2025-01-20" >
      <description>
        <p>Bugfix: Date format was not following system default</p>
        <ul>
					<li>Date format was not following system default</li>
				</ul>
      </description>
    </release>
	  <release version="0.5.2" date="2024-12-06" >
      <description>
        <p>Bugfix for usage with GTK 4.17</p>
        <ul>
					<li>Fix an issue with the app not starting under GTK 4.17</li>
				</ul>
      </description>
    </release>
	  <release version="0.5.1" date="2024-11-10" >
      <description>
        <p>Switch to the GNOME 47 runtime</p>
        <ul>
					<li>Switch to the GNOME 47 runtime</li>
				</ul>
      </description>
    </release>
	  <release version="0.5.0" date="2023-11-14" >
      <description>
        <p>UI redesign, adding French and better touch support</p>
        <ul>
					<li>Reduced input lag on touch screens significantly</li>
          <li>Complet UI redesign for a more compact view</li>
          <li>Add French translation</li>
          <li>Switch to the GNOME 45 runtime</li>
          <li>Rotate the information on the map if the map is rotated</li>
          <li>Fix a double scrolling bug which caused the map to be unresponsive</li>
          <li>App is now recognized as mobile friendly</li>
				</ul>
      </description>
    </release>
	  <release version="0.4.0" date="2023-04-23" >
      <description>
        <p>Bug fixes, UI improvements and GNOME 44 runtime</p>
        <ul>
					<li>Reduced input lag on touch screens</li>
          <li>Fullscreen map mode is now saved and not reseted after an app restart</li>
          <li>Dependency update</li>
					<li>Switch to GNOME 44 runtime</li>
				</ul>
      </description>
    </release>
		<release version="0.3.2" date="2022-10-01" >
      <description>
        <p>Design changes and switch to GNOME 43 runtime</p>
        <ul>
					<li>Switch to AdwAboutDialog to match GNOME 43 design</li>
					<li>Update Flatpak dependencies</li>
					<li>Switch to GNOME 43 runtime</li>
				</ul>
      </description>
    </release>
		<release version="0.3.1" date="2022-09-14" >
      <description>
        <p>Code cleanup and dependency changes</p>
        <ul>
					<li>Code cleanup of the time and date selector</li>
					<li>Switch to geocode-glib-2.0 to remove last libsoup-2 dependency</li>
				</ul>
      </description>
    </release>
		<release version="0.3.0" date="2022-09-12" >
      <description>
        <p>New features have been added:</p>
        <ul>
					<li>Add location service to find users current location</li>
					<li>Add support for languages</li>
					<li>Add German translation</li>
				</ul>
      </description>
    </release>
		<release version="0.2.0" date="2022-09-05" >
      <description>
        <p>UI improvements and smaller bug fixes</p>
        <ul>
					<li>Improve UI to fit better on mobile devices</li>
					<li>UI bug fixes</li>
					<li>Slight code improvements</li>
				</ul>
      </description>
    </release>
		<release version="0.1.0" date="2022-07-20">
      <description>
        <p>
          First official release.
        </p>
      </description>
    </release>
	</releases>

</component>
