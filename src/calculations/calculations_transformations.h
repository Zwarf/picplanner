/*
 * calculations_transformations.h
 * Copyright (C) 2021 Zwarf <zwarf@mail.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <time.h>
#include <math.h>
#include <glib.h>


/*
 * NUM_DATA_POINTS: The plots always show 24 hour. Therfore this value gives the amount of
 * datapoints which have to be calculated. (24*60)min / 5min = 288 --> Step every 5 min.
 */
#define NUM_DATA_POINTS 1440


double
calc_deg_to_rad (double deg);

double
calc_rad_to_deg (double rad);

int
times_to_time_zone (int day_utc,
                    int hour_utc,
                    int day_local,
                    int hour_local);

int
*get_time_utc (void);

int
*utc_time_to_local_time (int *time_utc);

int
*local_time_to_utc_time (int *time_local);

double
calc_jd (GDateTime *date_time);

double
time_jd_to_sidereal_time (double    longitude,
                          GDateTime *date_time);

double
*picplanner_transform_rotational_to_horizontal (double *coordinates_rot,
                                                double latitude,
                                                double time_sidereal);

int
*picplanner_get_index_rise_upper_set_lower (double *coordinates_array);

int
*picplanner_get_index_dark_blue_golden (double *coordinates_array);

void
picplanner_get_date_time_from_index (GDateTime **date_time_index,
																		 GDateTime **date_time_noon,
																		 int       index_time);
